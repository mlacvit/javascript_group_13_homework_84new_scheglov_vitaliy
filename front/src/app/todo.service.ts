import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';
import { TaskData, TaskModel } from './Models/task.model';
import { UserModel } from './Models/username.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient) { }

  createTask(taskData: TaskData) {
    return this.http.post('http://localhost:8000/task', taskData);
  };

  putTask(id: string, data: TaskData) {
    return this.http.put(`http://localhost:8000/task/${id}`, data);
  };

  remTask(id: string) {
    return this.http.delete(`http://localhost:8000/task/${id}`);
  };

  getTask() {
    return this.http.get<TaskModel[]>('http://localhost:8000/task').pipe(
      map(response => {
        return response.map(result => {
          return new TaskModel(
            result._id,
            result.title,
            result.user,
            result.status,
          );
        });
      })
    );
  }

  getUser() {
    return this.http.get<UserModel[]>('http://localhost:8000/user').pipe(
      map(response => {
        return response.map(result => {
          return new UserModel(
            result._id,
            result.username,
          );
        });
      })
    );
  }


}
