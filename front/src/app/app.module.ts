import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TaskEffects } from './Store/todo.effects';
import { taskReducer } from './Store/todo.reducer';
import { CreateComponent } from './create/create.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    EffectsModule.forRoot([TaskEffects]),
    StoreModule.forRoot({task: taskReducer}, {}),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
