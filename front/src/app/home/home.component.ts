import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskData, TaskModel } from '../Models/task.model';
import { Store } from '@ngrx/store';
import { AppState } from '../Store/types';
import { fetchTaskReq, putTaskReq, remTaskReq } from '../Store/todo.actions';
import { UserModel } from '../Models/username.model';
import { TodoService } from '../todo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  username!: UserModel[];
  task: Observable<TaskModel[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  taskData!: TaskData;
  constructor(private store: Store<AppState>,
              private service: TodoService,
              private router: Router) {
    this.task = store.select(state => state.task.task);
    this.loading = store.select(state => state.task.loading);
    this.error = store.select(state => state.task.fError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchTaskReq());
    this.service.getUser().subscribe(result => {
      this.username = result;
    })
  }


  sendUser(e: any, task: TaskModel) {
    const id = task._id;
    const user = e.target.value;
    const data: TaskData = {
     title: task.title,
     user: user,
      status: 'done'
    }
    this.store.dispatch(putTaskReq({id, data}));
  }

  rem(id: string) {
    this.store.dispatch(remTaskReq({id}));
  }
}
