export class TaskModel {
  constructor(
    public _id: string,
    public title: string,
    public user: string,
    public status: string,
  ) {}
}


export interface TaskData {
  title: string;
  user: string,
  status: string
}


