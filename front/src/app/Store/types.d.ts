import { TaskModel } from '../Models/task.model';

export type TaskState = {
  task: TaskModel[],
  loading: boolean,
  fError: null | string,
  createLoading: boolean,
  createError: null | string,
  putLoading: boolean,
  putError: null | string,
  remError: null | string,
  remLoading: boolean,
};

export type AppState = {
  task: TaskState
}
