import { createAction, props } from '@ngrx/store';
import { TaskData, TaskModel } from '../Models/task.model';

export const fetchTaskReq = createAction('[Task] Fetch Req');
export const fetchTaskSusses = createAction(
  '[Task] Fetch susses',
  props<{task: TaskModel[]}>()
);
export const fetchTaskFail = createAction(
  '[Task] Fetch Fail',
  props<{error: string}>()
);

export const createTaskReq = createAction(
  '[Task] create request',
  props<{taskData: TaskData}>()
);
export const createTaskSuc = createAction(
  '[Task] create susses'

);
export const createTaskFai = createAction(
  '[Task] create failure',
  props<{error: string}>()
);

export const putTaskReq = createAction(
  '[Task] put request',
  props<{id: string, data: TaskData}>()
);
export const putTaskSuc = createAction(
  '[Task] put susses'
);
export const putTaskFai = createAction(
  '[Task] put failure',
  props<{error: string}>()
);

export const remTaskReq = createAction(
  '[Task] delete request',
  props<{id: string}>()
);
export const remTaskSuc = createAction(
  '[Task] delete susses'

);
export const remTaskFai = createAction(
  '[Task] delete failure',
  props<{error: string}>()
);


