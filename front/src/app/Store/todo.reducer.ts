import { createReducer, on } from '@ngrx/store';
import { TaskState } from './types';
import {
  createTaskFai,
  createTaskReq,
  createTaskSuc,
  fetchTaskFail,
  fetchTaskReq,
  fetchTaskSusses,
  putTaskFai,
  putTaskReq,
  putTaskSuc,
  remTaskFai,
  remTaskReq,
  remTaskSuc
} from './todo.actions';

const initialState: TaskState = {
  task: [],
  loading: false,
  fError: null,
  createLoading: false,
  createError: null,
  putLoading: false,
  putError: null,
  remError: null,
  remLoading: false,
};

export const taskReducer = createReducer(
  initialState,
  on(fetchTaskReq, state => ({...state, loading: true})),
  on(fetchTaskSusses, (state, {task}) => ({
    ...state,
    loading: false,
    task
  })),
  on(fetchTaskFail, (state, {error}) => ({
    ...state,
    loading: false,
    fError: error
  })),
  on(createTaskReq, state => ({...state, createLoading: true})),
  on(createTaskSuc, state => ({...state, createLoading: false})),
  on(createTaskFai, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(putTaskReq, state => ({...state, putLoading: true})),
  on(putTaskSuc, (state) => ({...state, putLoading: false})),
  on(putTaskFai, (state, {error}) => ({
    ...state,
    putLoading: false,
    putError: error,
  })),
  on(remTaskReq, state => ({...state, remLoading: true})),
  on(remTaskSuc, state => ({...state, remLoading: false})),
  on(remTaskFai, (state, {error}) => ({
    ...state,
    remLoading: false,
    remError: error,
  })),
);

