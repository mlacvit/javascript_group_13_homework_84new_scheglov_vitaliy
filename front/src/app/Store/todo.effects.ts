import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, mergeMap, map, of, tap } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo.service';
import {
  createTaskFai,
  createTaskReq,
  createTaskSuc,
  fetchTaskFail,
  fetchTaskReq,
  fetchTaskSusses, putTaskFai, putTaskReq, putTaskSuc, remTaskFai, remTaskReq, remTaskSuc,
} from './todo.actions';


@Injectable()
export class TaskEffects {

  fetchTask = createEffect(() => this.actions.pipe(
    ofType(fetchTaskReq),
    mergeMap(() => this.service.getTask().pipe(
      map((task) => fetchTaskSusses({task})),
      catchError(() => of(fetchTaskFail({
        error: 'Something went wrong'
      })))
    ))
  ));

  createTask = createEffect(() => this.actions.pipe(
    ofType(createTaskReq),
    mergeMap(({taskData}) => this.service.createTask(taskData).pipe(
      map(() => createTaskSuc()),
      tap(() => {
        this.router.navigate(['/']);
      }),
      catchError(() => of(createTaskFai({error: 'wrong data'})))
    ))
  ));

  putTask = createEffect(() => this.actions.pipe(
    ofType(putTaskReq),
    mergeMap(({id, data}) => this.service.putTask(id, data).pipe(
      map(() => putTaskSuc()),
      tap(() => {
        this.service.getTask();
        this.router.navigate(['/']);
      }),
      catchError(() => of(putTaskFai({error: 'wrong data'})))
    ))
  ));

  remTask = createEffect(() => this.actions.pipe(
    ofType(remTaskReq),
    mergeMap(({id}) => this.service.remTask(id).pipe(
      map(() => remTaskSuc()),
      tap(() => {
        this.service.getTask();
        this.router.navigate(['/']);
      }),
      catchError(() => of(remTaskFai({error: 'wrong data'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private service: TodoService,
    private router: Router,
  ) {}
}
