import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../Store/types';
import { TaskData } from '../Models/task.model';
import { createTaskReq } from '../Store/todo.actions';
import { TodoService } from '../todo.service';
import { UserModel } from '../Models/username.model';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  username!: UserModel[];
  constructor(private store: Store<AppState>, private service: TodoService) {
    this.loading = store.select(state => state.task.createLoading);
    this.error = store.select(state => state.task.createError);
  }

  ngOnInit(): void {
    this.service.getUser().subscribe(result => {
      this.username = result;
    })
  }
  onSub() {
    const taskData: TaskData = this.form.value;
    this.store.dispatch(createTaskReq({taskData}));
  }

}
