const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  title: {
    type: String,
    required: true
  },

  user: String,

  status: String,

});

const Task = mongoose.model('task', TaskSchema);

module.exports = Task;