const express = require('express');
const router = express.Router();
const userName = require('./models/Username');


router.get('/', async (req, res, next) => {
  try {
    const users = await userName.find();
    return res.send(users);
  } catch (e) {
    next(e);
  }
});


router.post('/', async (req, res, next) => {
  try {
    if (!req.body.username) {
      return res.status(400).send({message: 'artist are required'});
    }

    const user = {
      username: req.body.username,
    };

    const userCreate = new userName(user);

    await userCreate.save();

    return res.send({message: 'Created new product', id: userCreate._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;