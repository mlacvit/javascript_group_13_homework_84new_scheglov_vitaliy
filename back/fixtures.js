const mongoose = require('mongoose');
const config = require('./config');
const Username = require('./models/Username');
const Tasks = require('./models/Tasks');
const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [john, jane] = await Username.create({
    username: 'John'
  },
    {
      username: 'Jane'
    })

  await Tasks.create({
    title: 'hello world',
    user: john,
    status: 'new'
  },
    {
      title: 'hello John',
      user: jane,
      status: 'new'
    })

  await mongoose.connection.close();
};


run().catch(e => console.error(e));