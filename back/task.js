const express = require('express');
const router = express.Router();
const task = require('./models/Tasks');
const username = require('./models/Username');

router.get('/', async (req, res, next) => {
  try {
    const sort = {};
    if (req.query.orderBy === 'date' && req.query.direction === 'desc') {
      sort._id = -1;
    }
    const tasks = await task.find().sort(sort);
    tasks.user = username;
    return res.send(tasks);
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
  const tasks = await task.findByIdAndUpdate(
    `${req.params.id}`,
    {$set:req.body},
    (err, result) => {
    if(err){
      console.log(err);
    }
    return result;
  });
    const tasksInfo = new task(tasks);
    tasksInfo.status = 'done';
    await tasksInfo.save();

  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    return task.findById(`${req.params.id}`).deleteMany();
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'title are required'});
    }

    const tasks = {
      title: req.body.title,
      user: 'N/A'
    };

    if (req.body.user){
      const users = await username.findById(`${req.body.user}`);
      tasks.user = users.username;
    }
    tasks.status = 'new';

    const tasksInfo = new task(tasks);

    await tasksInfo.save();

    return res.send({message: 'Created new task', id: tasksInfo._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;